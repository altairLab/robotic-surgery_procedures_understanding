#This script prepares result files for frame-argument evaluation.
#Then use the same perl script to evaluate
import pandas as pd
import os
#==============================#
#frame_path_PRED = os.path.join("FFT_roberta", "frame.gold.txt")
FOLDER="[RESULTS_FOLDER]"
frame_path_PRED = os.path.join(FOLDER, "PRED_or_GOLD_file.txt")
#result_path_PRED = os.path.join("FFT_roberta", "prova.gold.txt")
result_path_PRED = os.path.join(FOLDER, "results.pred.txt")
OUTPUT=os.path.join(FOLDER, "RESULTS_"+result_path_PRED[-8:])
frames_PRED = list()
with open(frame_path_PRED, 'r', encoding='utf-8') as f:
    for line in f:
        frames_PRED.append(line)
#==============================#
def get_string_to_append(s,f):
    output=""
    if s[-1]==")":
        output = " " + s[:-2]+"-"+str(f)+'*)'
    else:
        output = " " + s[:-1] +"-"+str(f)+'*'
    return output    
#==============================#
number_of_arguments_in_this_sentence = 0
with open(result_path_PRED, 'r', encoding='utf-8') as file:
    current_sentence = 0
    current_frame_index = 0
    final_output = ""
    for line in file:
        text_to_print = ""
        if line == "\n":
            text_to_print = text_to_print + "\n"
            current_frame_index += number_of_arguments_in_this_sentence
        else:
            fields = line.split()
            number_of_arguments_in_this_sentence = len(fields)-1
            for i, f in enumerate(fields):
                if i==0:
                    text_to_print += f
                else:    
                    if  "(ARG" in f or "(R-ARG" in f:
                        current_frame = int(frames_PRED[current_frame_index + i - 1])
                        text_to_print += get_string_to_append(f, current_frame)
                    else:
                        text_to_print += " "+f
              
            text_to_print = text_to_print + "\n"
        final_output += text_to_print
    #print(final_output)
        
with open(OUTPUT, "w", encoding='utf-8') as text_file:
    text_file.write(final_output)

#!/bin/sh
# ------------------------ BERT_TYPE ------------------------ #
#--bert_type can be a local directory or an HuggingFace model. In this paper we used:
# allenai/biomed_roberta_base ---> HuggingFace
# roberta-base ---> HuggingFace
# SurgicBerta/ ---> Local Directory (see link in this repo), soon on HuggingFace

# ------------------------ DIR ------------------------ #
# --dir is the local directory generated during the pre-processing steps

echo "TRAINING"
#
GPUID=0
DROP=0.5
USE_GOLD=1
LR=0.00003
EPOCH=30
LOSS=crf
PERC=1
SEED=1
MODEL=./MY_MODELS/MODEL_NAME
python3 -u train.py --gpuid $GPUID --dir ./data/srl_bal/ --train_data conll2012.train.hdf5 --val_data conll2012.val.hdf5 \
	--train_res conll2012.train.orig_tok_grouped.txt,conll2012.train.frame.hdf5,conll2012.frame_pool.hdf5 \
	--val_res conll2012.val.orig_tok_grouped.txt,conll2012.val.frame.hdf5,conll2012.frame_pool.hdf5 \
	--label_dict conll2012.label.dict \
	--bert_type roberta-base --loss $LOSS  --epochs $EPOCH --learning_rate $LR --dropout $DROP \
	--percent $PERC --seed $SEED \
	--conll_output $MODEL --save_file $MODEL | tee ${MODEL}.txt
#
DROP=0.5
LR=0.00001
EPOCH=5
PERC=1
LOSS=crf,unique_role,frame,overlap_role
LAMBD=1,1,1,0.1
SEED=1
LOAD=./MY_MODELS/MODEL_NAME
MODEL=./MY_MODELS/MODEL_NAME_2_ROUND
python3 -u train.py --gpuid $GPUID --dir ./data/srl_bal/ --train_data conll2012.train.hdf5 --val_data conll2012.val.hdf5 \
	--train_res conll2012.train.orig_tok_grouped.txt,conll2012.train.frame.hdf5,conll2012.frame_pool.hdf5 \
	--val_res conll2012.val.orig_tok_grouped.txt,conll2012.val.frame.hdf5,conll2012.frame_pool.hdf5 \
	--label_dict conll2012.label.dict \
	--bert_type roberta-base --loss $LOSS --epochs $EPOCH --learning_rate $LR --dropout $DROP --lambd $LAMBD \
	--percent $PERC --seed $SEED \
	--load $LOAD --conll_output ${MODEL} --save_file $MODEL | tee ${MODEL}.txt

echo "EVALUATION"
DROP=0.5
LR=0.00001
EPOCH=5
SEED=1
PERC=1
LOSS=crf,unique_role,frame,overlap_role
LAMBD=1,1,1,0.1
TEST=test1
MODEL=./MY_MODELS/MODEL_NAME_2_ROUND
python3 -u eval.py --gpuid $GPUID --dir ./data/srl_bal/ --data conll2012.${TEST}.hdf5 \
	--res conll2012.${TEST}.orig_tok_grouped.txt,conll2012.${TEST}.frame.hdf5,conll2012.frame_pool.hdf5 \
	--label_dict conll2012.label.dict \
	--bert_type roberta-base --loss $LOSS --lambd $LAMBD \
	--load_file ${MODEL} --conll_output ${MODEL} | tee ${MODEL}.testlog.txt

#PRINTING EVALUATION
perl srl-eval.pl ${MODEL}.gold.txt ${MODEL}.pred.txt
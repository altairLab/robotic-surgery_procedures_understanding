# Machine understanding surgical actions from intervention procedure textbooks
Web page of our paper submitted to the journal *Computers in Biology and Medicine*.
Part of the code is taken from the work of [Li et al](https://github.com/utahnlp/structured_tuning_srl).
*This page is still under construction and will be updated soon*.

## In a nutshell
The automatic extraction of procedural surgical knowledge from surgery manuals, academic papers or other high-quality textual resources, is of the utmost importance to develop knowledge-based clinical decision support systems, to automatically execute some procedure’s step or to summarize the procedural information, spread throughout the texts, in a structured form usable as a study resource by medical students. In this work, we propose a first benchmark on extracting detailed surgical actions from available intervention procedure textbooks and papers. We frame the problem as a Semantic Role Labeling task. Exploiting a manually annotated dataset, we apply different Transformer-based information extraction methods. Starting from RoBERTa and BioMedRoBERTa pre-trained
language models, we first investigate a zero-shot scenario and compare the obtained results with a full fine-tuning setting. We then introduce an new ad-hoc surgical language model, named SurgicBERTa, pre-trained on a large collection of surgical materials, and we compare it with the previous ones. We also investigate the effectiveness of the approach in a few-shot learning scenario. Performance are evaluated on three correlated sub-tasks: predicate disambiguation, semantic argument disambiguation and predicate-argument disambiguation. Results show that the fine-tuning of a pre-trained domain-specific language model achieves the highest performance on all splits and on all sub-tasks. 

## Obtaining and preprocessing datasets
This paper uses two dataset for training and validation:
* Ontonotes 5.0 release of propbank data obtainable from official web page
* The Robotic-Surgery Propositional Bank (RSPB), obtainable [here](https://gitlab.com/altairLab/robotic-surgery-propositional-bank).

The datasets must be pre-processed following instructions of Li et al. In particular:
* *Extracting The Robotic-Surgery Propositional FrameBank*: [The Robotic-Surgery Propositional FrameBank](https://gitlab.com/altairLab/robotic-surgery-propositional-bank/-/tree/main/Robotic_Surgery_Procedural_Framebank) must be used instead of standard PropBank's because the robotic-surgical domain extends the general-English domain with new lemmas, frames and semantic roles. This framebank must be used both for zero-shot, few-shot and full fine-tuning scenarios.
* *Preprocessing CoNLL 2012* : follow the corresponding Li et al.'s instructions. In full-fine tuning and few-shot scenarios, train, validation and test RSPB's splits must be in a sub-directory of Ontonotes 5.0. In the zero-shot scenario only PropBank data must be in train and validation directories. Ontonotes's testing material should be removed from the corresponding directory in all scenarios, since we are only evaluating models on RSPB. 

## Training and Evaluation
To train and evaluate the SRL in the standard way (arguments evaluation), exec the script "train_and_argument_evaluation_script.sh", by indicating models you want to fine-tune. Roberta models used in this paper are: roberta-base, allenai/biomed_roberta_base and [SurgicBERTa](https://drive.google.com/file/d/1vQvj_qu33tIyxI-1ZiVgHpX_3POqqEkx/view?usp=sharing), also available on [Hugging Face](https://huggingface.co/marcobombieri/surgicberta).
To evaluate the models on frame or frame-argument disambiaguation tasks, use the "frame_argument_script" before running perl script.
